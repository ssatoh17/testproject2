<?php

$request = new HttpRequest();
$request->setUrl('https://api.sandbox.paypal.com/v1/catalogs/products');
$request->setMethod(HTTP_METH_POST);

$request->setHeaders(array(
  'cache-control' => 'no-cache',
  'Connection' => 'keep-alive',
  'Content-Length' => '290',
  'Accept-Encoding' => 'gzip, deflate',
  'Host' => 'api.sandbox.paypal.com',
  'Postman-Token' => '45c07ceb-13af-47a5-9c2f-6da36991b249,c71b91f5-e1c5-446b-8abd-5c9861d0bcd0',
  'Cache-Control' => 'no-cache',
  'Accept' => '*/*',
  'User-Agent' => 'PostmanRuntime/7.20.1',
  'Authorization' => 'Bearer A21AAHS06w_hlIb5K1WTlehYQqYsm-uixPYZpnatkM93j7TfdC0vaFqLm_4JJ-gq230jk7ND5Ef7IDMAr-VJGN9ynnNKJ5qQw',
  'Content-Type' => 'application/json'
));

$request->setBody('{
  "name": "商品名/サービス名",
  "description": "Video streaming service",
  "type": "SERVICE",
  "category": "SOFTWARE",
  "image_url": "https://gyazo.com/e1b67adbac214f58c7053e373dfd9935.png",
  "home_url": "https://www.paypal.com/apex/product-profile/ordersv2/getAccessToken"
}');

try {
  $response = $request->send();

  echo $response->getBody();
} catch (HttpException $ex) {
  echo $ex;
}